import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Input an integer whose factorial will be computed: ");
        int num = 0;

        //Exception Handling
        try{
            Scanner in = new Scanner(System.in);
            num = in.nextInt();
        } catch(Exception e){
            System.out.println("Invalid Input");
        }

        int answer = 1;
        int counter = 1;

        //WHILE LOOP
/*        while(num >= counter){
//            System.out.println("current counter = " + counter);
//            System.out.println("curernt answer = " + answer);
            answer *= counter;
//            System.out.println("new answer = " + answer);
            counter++;
        }
        System.out.println("The factorial of " + num + " is " + answer);*/

        //FOR-LOOP
        for(counter = 1; counter <= num; counter++){
//            System.out.println("current counter: " + counter);
//            System.out.println("current answer: " + answer);
            answer *= counter;
//            System.out.println("new answer: " + answer);
        }
        System.out.println("The factorial of " + num + " is " + answer);
    }
}